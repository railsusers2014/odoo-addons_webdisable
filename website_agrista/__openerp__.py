{
    'name': 'Agrista Website Pages',
    'version': '1.0',
    'category': 'Agrista',
    "sequence": 14,
    'complexity': "easy",
    'description': """
        Add Agrista marketing pages to the public web site.
    """,
    'author': 'Agrista (Pty) Ltd',
    'website': 'www.agrista.com',
    'data': [
        'views/snippets.xml',
        'views/website_agrista.xml',
        'views/snippets/s_carousel.xml',
        'views/snippets/s_masthead.xml',
        'views/snippets/s_coming_soon.xml',
        'views/snippets/s_features.xml'
    ],
    'depends': [
        'website'
    ],
    'installable': True,
    'auto_install': False,
}
