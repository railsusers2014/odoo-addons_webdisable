==================
Custom Odoo Addons
==================

This is where we code addons to Odoo.

To get coding in your odoo folder:

.. code-block:: console

	sudo ln -s ../odoo-addons custom
	./odoo.py --addons-path addons,custom/addons

Agrista Theme
-------------

To modify the theme, change into the the theme directory:

.. code-block:: console

	cd addons/theme_agrista

then run the sass watcher task:

.. code-block:: console

	grunt watcher