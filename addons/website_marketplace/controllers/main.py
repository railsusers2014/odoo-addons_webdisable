# -*- coding: utf-8 -*-

from openerp.addons.website_sale.controllers import main
from openerp import http
from openerp.http import request
import logging
import requests
import json
from openerp.modules.registry import RegistryManager

_logger = logging.getLogger(__name__)

consent_fields = ['consent_delivery_location', 'consent_farm_gate']

partner_tags_map = {
    'tags_farmer_grain': 2,
    'tags_farmer_fruit': 3,
    'tags_farmer_vegetables': 4,
    'tags_farmer_dairy': 5,
    'tags_farmer_sugarcane': 6,
    'tags_supplier_fertiliser': 8,
    'tags_supplier_seed': 9,
    'tags_supplier_agrochemicals': 10,
    'tags_supplier_diesel': 11,
    'tags_supplier_spare_parts': 12,
    'tags_offtaker_fruit': 14,
    'tags_offtaker_vegetables': 15,
    'tags_offtaker_livestock': 16,
    'tags_offtaker_milk': 17,
    'tags_offtaker_sugarcane': 18
}


def update_partner_tags(partner, tags, existing_categories):
    partner_tags = []
    for tag in tags:
        if tag not in existing_categories:
            partner_tags.append(tag)

    if partner_tags:

        UPDATE_TAGS = "INSERT INTO res_partner_res_partner_category_rel VALUES"
        for tag in partner_tags:
            UPDATE_TAGS = UPDATE_TAGS + " (" + str(tag) + "," + str(partner.id) + "),"
        UPDATE_TAGS = UPDATE_TAGS[:-1]

        registry = RegistryManager.get(request.session.db)
        cr = registry.cursor()
        cr.execute(UPDATE_TAGS)
        cr.close()


def consent_given(data=None):
    error = {}
    consent = True
    if not data:
        for field in consent_fields:
            error[field] = True
            consent = False
    else:
        for field in consent_fields:
            if field not in data:
                error[field] = True
                consent = False

    values = {
        'consent': consent,
        'error': error,
    }

    return values


def login(hostname):
    _logger.debug("logging in")
    s = requests.Session()
    url = str(hostname) + "web/login"
    # credentials = {'login': 'intercompany@agrista.com', 'password': 'changeme'}
    credentials = {'login': 'admin', 'password': 'changeme'}
    _logger.debug(url)
    s.post(url, data=credentials)
    return s


def contains_subscription(order):
    order_lines = order.order_line

    for line in order_lines:
        if line.product_id.type == 'sub':
            return True

    return False


def get_user_id(s, email, name, hostname):
    response = search_user(s, email, hostname)
    if response['result']['length'] > 0:
        return response['result']['records'][0]['id']
    else:
        response = create_user(s, email, name, hostname)
        return response['result']


def search_user(s, email, hostname):
    url = hostname + "web/dataset/search_read"
    headers = {'content-type': 'application/json'}
    payload = {
        "id": 0,
        "jsonrpc": "2.0",
        "method": "call",
        "params": {
            "context": {
                "bin_size": True,
                "lang": "en_US",
                "search_default_no_share": 1,
                "tz": False,
                "uid": 1
            },
            "domain": [
                '|',
                [
                    'login',
                    '=',
                    email
                ],
                [
                    "email",
                    "=",
                    email
                ]
            ],
            "fields": [
                "name",
                "login",
                "lang",
                "login_date"
            ],
            "limit": 80,
            "model": "res.users",
            "offset": 0,
            "sort": ""
        }
    }
    r = s.post(url, data=json.dumps(payload), headers=headers)
    return r.json()


def name_search(self, name, args=None, operator='ilike', limit=100):
    args = args or []
    if name:
        # Be sure name_search is symetric to name_get
        name = name.split(' / ')[-1]
        args = [('name', operator, name)] + args
    categories = self.search(args, limit=limit)
    return categories.name_get()


def create_user(s, email, name, hostname):
    url = hostname + "web/dataset/call_kw/res.users/create"
    headers = {'content-type': 'application/json'}
    payload = {
        "id": 0,
        "method": "call",
        "jsonrpc": "2.0",
        "params": {
            "args": [{
                "action_id": False,
                "active": True,
                "alias_contact": "everyone",
                "alias_id": False,
                "alias_name": False,
                "company_id": 1,
                "company_ids": [
                    [
                        6,
                        False,
                        [
                            1
                        ]
                    ]
                ],
                "default_section_id": False,
                "display_groups_suggestions": True,
                "email": email,
                "in_group_1": False,
                "in_group_11": True,
                "in_group_16": False,
                "in_group_17": False,
                "in_group_18": False,
                "in_group_2": False,
                "in_group_20": False,
                "in_group_21": False,
                "in_group_6": False,
                "in_group_7": False,
                "in_group_8": False,
                "lang": "en_US",
                "login": email,
                "name": name,
                "notify_email": "always",
                "sel_groups_12": False,
                "sel_groups_13": False,
                "sel_groups_14_15": False,
                "sel_groups_3_4": False,
                "sel_groups_5": 5,
                "sel_groups_9_19_10": False,
                "signature": False,
                "tz": False
            }],
            "kwargs": {
                "context": {
                    "lang": "en_US",
                    "search_default_no_share": 1,
                    "tz": False,
                    "uid": 1
                }
            },
            "method": "create",
            "model": "res.users"
        }
    }
    r = s.post(url, data=json.dumps(payload), headers=headers)
    return r.json()


def follow_blog(blog_id, email, hostname):
    url = hostname + "website_mail/follow"
    headers = {'content-type': 'application/json'}
    payload = {
        "id": 0,
        "jsonrpc": "2.0",
        "method": "call",
        "params": {
            "email": email,
            "id": blog_id,
            "message_is_follower": "off",
            "object": "blog.blog"
        }
    }
    requests.post(url, data=json.dumps(payload), headers=headers)


def get_categories(user_record):
    return [c.id for c in user_record.partner_id.category_id]


class AgristaSale(main.website_sale):
    @http.route(auth='user')
    def checkout(self, **post):
        context = request.context

        order = request.website.sale_get_order(force_create=1, context=context)

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        values = self.checkout_values()
        # Add boolean to indicate whether the order contains a subscription product
        values['contains_subscription'] = contains_subscription(order)
        return request.website.render("website_sale.checkout", values)

    @http.route(auth='user')
    def confirm_order(self, **post):
        order = request.website.sale_get_order(context=request.context)
        if contains_subscription(order):

            consent_values = consent_given(post)
            values = self.checkout_values(post)
            values['contains_subscription'] = True
            values["error"] = self.checkout_form_validate(values["checkout"])
            if not consent_values['consent']:
                values['error']['consent'] = False

            for field in consent_fields:
                if field in consent_values['error']:
                    values['error'][field] = 'missing'
                if field in post:
                    values['checkout'][field] = post[field]

            if values["error"]:
                return request.website.render("website_sale.checkout", values)

            # update partner tags
            tags = []
            for key in post.keys():
                if key in partner_tags_map:
                    tags.append(partner_tags_map[key])

            user_record = request.registry.get('res.users').browse(request.cr, request.uid, request.uid)
            partner = user_record.partner_id
            existing_categories = get_categories(user_record)
            update_partner_tags(partner, tags, existing_categories)

        return super(AgristaSale, self).confirm_order(**post)

    @http.route(auth='user')
    def payment_validate(self, transaction_id=None, sale_order_id=None, **post):
        order = request.website.sale_get_order(context=request.context)

        # if order.partner_id.credit_limit < (order.amount_total +
        #                                     order.partner_id.credit):
        #     _logger.info("insufficient funds!")
        #     return request.redirect('/shop/payment')

        if contains_subscription(order):
            registry = RegistryManager.get(request.session.db)
            cr = registry.cursor()
            lead_params_active = True
            lead_params_city = order.partner_id.city or ''
            lead_params_company_id = 1
            lead_params_contact_name = order.partner_id.name or ''
            lead_params_country_id = order.partner_id.country_id.id
            lead_params_email_from = order.partner_id.email or ''
            lead_params_fax = order.partner_id.fax or ''
            lead_params_mobile = order.partner_id.mobile or ''
            lead_params_name = order.product_id.name
            lead_params_partner_id = order.partner_id.id
            lead_params_partner_name = order.partner_id.name or ''
            lead_params_phone = order.partner_id.phone or ''
            lead_params_stage_id = 1
            # lead_params_state_id = order.partner_id.state_id.id
            lead_params_street = order.partner_id.street or ''
            lead_params_street2 = order.partner_id.street2 or ''
            lead_params_type = 'lead'
            lead_params_zip = order.partner_id.zip or ''
            cr.execute("INSERT INTO crm_lead"
                       " (active, city, company_id, contact_name, country_id, email_from, fax, mobile, name, partner_id, partner_name, phone, stage_id"
                       ", street, street2, type, zip, date_action_last, create_date)"
                       " VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, now(), now())"
                       , (lead_params_active, lead_params_city, lead_params_company_id, lead_params_contact_name,
                          lead_params_country_id, lead_params_email_from,
                          lead_params_fax, lead_params_mobile, lead_params_name, lead_params_partner_id,
                          lead_params_partner_name, lead_params_phone,
                          lead_params_stage_id, lead_params_street, lead_params_street2, lead_params_type,
                          lead_params_zip,))
            cr.close()

            # TODO: modify create_lead to associate the partner record to the lead.
            # user_record = request.registry.get('res.users').browse(request.cr,
            #                                                        request.uid,
            #                                                        request.uid)
            # user_id = get_user_id(session, user_record.email, user_record.name,
            #                       hostname)
            # if user_id:
            #     _logger.info("user exists")

            # TODO: find out the blog id programatically
            # follow_blog(2, user_record.email, hostname)

        return super(AgristaSale, self).payment_validate(transaction_id, sale_order_id, **post)




