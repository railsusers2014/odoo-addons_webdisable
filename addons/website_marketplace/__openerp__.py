# -*- coding: utf-8 -*-
{
    'name': 'Agrista Marketplace',
    'version': '1.0',
    'category': 'Agrista',
    "sequence": 14,
    'complexity': "easy",
    'description': """
        Add marketplace features to the Agrista website..
    """,
    'author': 'Agrista (Pty) Ltd',
    'website': 'www.agrista.com',
    'depends': ['website_sale', 'crm'],
    'data': [
        'ir.model.access.csv',
        'views/pages.xml',
        'data/data.xml',
    ],
    'qweb' : [
        "static/src/xml/*.xml",
    ],
    'installable': True,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
