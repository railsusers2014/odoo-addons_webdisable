from openerp.osv import orm
from openerp import fields
from openerp import models
from openerp.http import request
from ldap.filter import filter_format
import ldap
import logging

_logger = logging.getLogger(__name__)

class Enterprise(models.Model):
    _name = 'website_marketplace.enterprises'

    name = fields.Char()
    code = fields.Char()
    type = fields.Selection([('FC', 'Field Crops'), ('HC', 'Horticulture'),
                             ('LS', 'Livestock')], 'Enterprise Type',
                            required=True)
    market_update_ids = fields.One2many('product.template', 'enterprise_id',
                                        string='Market Updates')


class MarketUpdates(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    enterprise_id = fields.Many2one('website_marketplace.enterprises',
                                    string="Enterprise")
    type = fields.Selection([('product', 'Stockable Product'),
                             ('consu', 'Consumable'), ('service', 'Service'),
                             ('sub', 'Subscription')],
                            'Product Type', required=True,
                            help=("Consumable: Will not imply stock "
                                  "management for this product.\n"
                                  "Stockable product: Will imply stock "
                                  "management for this product.\n"
                                  "Subscription: Will imply lead creation and "
                                  "blog following for this product."))


class web_sale_helper(orm.Model):
    _inherit = 'website'

    def get_ldap_dicts(self, cr, ids=None):
        """
        Retrieve res_company_ldap resources from the database in dictionary
        format.

        :param list ids: Valid ids of model res_company_ldap. If not \
        specified, process all resources (unlike other ORM methods).
        :return: ldap configurations
        :rtype: list of dictionaries
        """

        if ids:
            id_clause = 'AND id IN (%s)'
            args = [tuple(ids)]
        else:
            id_clause = ''
            args = []
        cr.execute("""
            SELECT id, company, ldap_server, ldap_server_port, ldap_binddn,
                   ldap_password, ldap_filter, ldap_base, "user", create_user,
                   ldap_tls
            FROM res_company_ldap
            WHERE ldap_server != '' """ + id_clause + """ ORDER BY sequence
        """, args)
        return cr.dictfetchall()

    def connect(self, conf):
        """
        Connect to an LDAP server specified by an ldap
        configuration dictionary.

        :param dict conf: LDAP configuration
        :return: an LDAP object
        """

        uri = 'ldap://%s:%d' % (conf['ldap_server'],
                                conf['ldap_server_port'])

        connection = ldap.initialize(uri)
        if conf['ldap_tls']:
            connection.start_tls_s()
        return connection

    def query(self, conf, filter, retrieve_attributes=None):
        """
        Query an LDAP server with the filter argument and scope subtree.

        Allow for all authentication methods of the simple authentication
        method:

        - authenticated bind (non-empty binddn + valid password)
        - anonymous bind (empty binddn + empty password)
        - unauthenticated authentication (non-empty binddn + empty password)

        .. seealso::
           :rfc:`4513#section-5.1` - LDAP: Simple Authentication Method.

        :param dict conf: LDAP configuration
        :param filter: valid LDAP filter
        :param list retrieve_attributes: LDAP attributes to be retrieved. \
        If not specified, return all attributes.
        :return: ldap entries
        :rtype: list of tuples (dn, attrs)

        """

        results = []
        try:
            conn = self.connect(conf)
            conn.simple_bind_s(conf['ldap_binddn'] or '',
                               conf['ldap_password'] or '')
            results = conn.search_st(conf['ldap_base'], ldap.SCOPE_SUBTREE,
                                     filter, retrieve_attributes, timeout=60)
            conn.unbind()
        except ldap.INVALID_CREDENTIALS:
            _logger.error('LDAP bind failed.')
        except ldap.LDAPError, e:
            _logger.error('An LDAP exception occurred: %s', e)
        return results

    def authenticate(self, conf, login, password):
        """
        Authenticate a user against the specified LDAP server.

        In order to prevent an unintended 'unauthenticated authentication',
        which is an anonymous bind with a valid dn and a blank password,
        check for empty passwords explicitely (:rfc:`4513#section-6.3.1`)

        :param dict conf: LDAP configuration
        :param login: username
        :param password: Password for the LDAP user
        :return: LDAP entry of authenticated user or False
        :rtype: dictionary of attributes
        """

        if not password:
            return False

        entry = False
        filter = filter_format(conf['ldap_filter'], (login,))
        try:
            results = self.query(conf, filter)

            # Get rid of (None, attrs) for searchResultReference replies
            results = [i for i in results if i[0]]
            if results and len(results) == 1:
                dn = results[0][0]
                conn = self.connect(conf)
                conn.simple_bind_s(dn, password)
                conn.unbind()
                entry = results[0]
        except ldap.INVALID_CREDENTIALS:
            return False
        except ldap.LDAPError, e:
            _logger.error('An LDAP exception occurred: %s', e)
        return entry

    def get_company_name_by_dn(self, dn):
        company_name = False
        if dn and 'o=' in dn:
            company_name = dn.split(",")[1][2:]
        return company_name

    def get_user_org_dn(self, conf, ldap_entry):
        dn = False
        uid = ldap_entry[1]['uid'][0] or False
        ldap_base_for_org = 'dc=agrista,dc=com'
        if uid:
            filter = filter_format('uniquemember=uid=%s,ou=people,dc=agrista,dc=com', (uid,))
            query_results = []
            try:
                conn = self.connect(conf)
                conn.simple_bind_s(conf['ldap_binddn'] or '',
                                   conf['ldap_password'] or '')
                query_results = conn.search_st(ldap_base_for_org, ldap.SCOPE_SUBTREE,
                                               filter, None, timeout=60)
            except ldap.INVALID_CREDENTIALS:
                _logger.error('LDAP bind failed.')
            except ldap.LDAPError, e:
                _logger.error('An LDAP exception occurred: %s', e)
            if query_results and len(query_results) == 1:
                dn = query_results[0][0]
        return dn

    def get_user_org_by_name(self, conf, company_name):
        company_entry = False
        ldap_base_for_org = 'dc=agrista,dc=com'
        if company_name:
            filter = filter_format('o=%s', (company_name,))
            query_results = []
            try:
                conn = self.connect(conf)
                conn.simple_bind_s(conf['ldap_binddn'] or '',
                                   conf['ldap_password'] or '')
                query_results = conn.search_st(ldap_base_for_org, ldap.SCOPE_SUBTREE,
                                               filter, None, timeout=60)
            except ldap.INVALID_CREDENTIALS:
                _logger.error('LDAP bind failed.')
            except ldap.LDAPError, e:
                _logger.error('An LDAP exception occurred: %s', e)
            if query_results and len(query_results) == 1:
                company_entry = query_results[0]
        return company_entry

    def get_user_org_name(self, conf, ldap_entry):
        dn = self.get_user_org_dn(conf, ldap_entry)
        return self.get_company_name_by_dn(dn)

    def helper_test(self):
        result_object = {
            'company': '',
            'email': '',
            'phone': '',
            'street': '',
            'zip': '',
            'categoryGroups': ''
        }

        helper_login = request.session['login']
        helper_password = request.session['password']
        helper_ldap_obj = request.registry['res.company.ldap']
        helper_cr = request.registry.cursor()

        helper_cr.execute("""SELECT id, name, parent_id FROM res_partner_category WHERE active = True ORDER BY id""", [])
        helper_category_list = helper_cr.fetchall()

        helper_category_groups = []
        for category in helper_category_list:
            if category[2] is None:
                new_group = {
                    "groupId": category[0],
                    "groupName": category[1],
                    "groupItems": []
                }
                helper_category_groups.append(new_group)

        for group in helper_category_groups:
            for category in helper_category_list:
                if category[2] == group['groupId']:
                    group['groupItems'].append(category)

        for conf in helper_ldap_obj.get_ldap_dicts(helper_cr):
            entry = helper_ldap_obj.authenticate(conf, helper_login, helper_password)
            if entry:
                entry_data_object = entry[1]
                uid = entry[1]['uid'][0] or False
                # get company info
                company_name = self.get_user_org_name(conf, entry)
                company_entry = self.get_user_org_by_name(conf, company_name)
                if company_entry:
                    company_entry_object = company_entry[1]
                    company = company_entry_object.get('o')[0] if company_entry_object.get('o') else ''
                    email = entry_data_object.get('mail')[0] if entry_data_object.get('mail') else ''
                    phone = ''
                    street = ''
                    postal_code = ''
                    if entry_data_object.get('telephoneNumber'):
                        phone = entry_data_object.get('telephoneNumber')[0]
                    elif company_entry_object.get('telephoneNumber'):
                        phone = company_entry_object.get('telephoneNumber')[0]
                    if entry_data_object.get('street'):
                        street = entry_data_object.get('street')[0]
                    elif company_entry_object.get('street'):
                        street = company_entry_object.get('street')[0]
                    if entry_data_object.get('postalCode'):
                        postal_code = entry_data_object.get('postalCode')[0]
                    elif company_entry_object.get('postalCode'):
                        postal_code = company_entry_object.get('postalCode')[0]

                    category_groups = helper_category_groups
                    result_object['company'] = company
                    result_object['email'] = email
                    result_object['phone'] = phone
                    result_object['street'] = street
                    result_object['postal_code'] = postal_code
                    result_object['categoryGroups'] = category_groups
        helper_cr.close()

        return result_object