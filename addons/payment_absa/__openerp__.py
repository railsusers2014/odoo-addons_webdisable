# -*- coding: utf-8 -*-

{
    'name': 'Absa Payment Acquirer',
    'category': 'Hidden',
    'summary': 'Payment Acquirer: Absa Implementation',
    'version': '1.0',
    'description': """Absa Payment Acquirer""",
    'author': 'Agrista',
    'depends': ['payment'],
    'data': [
        'views/absa.xml',
        'views/payment_acquirer.xml',
        'views/res_config_view.xml',
        'data/absa.xml',
    ],
    'installable': True,
}
