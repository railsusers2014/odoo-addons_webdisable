$(window).scroll(function() {
    if ($(document).scrollTop() > 150) {
        $('.navbar').addClass('shrink');
    }
    else {
        $('.navbar').removeClass('shrink'); }
});

$('#nav').affix({
    offset: {
        top: $('header').height() + $('#home_carousel').height() - $('#oe_main_menu_navbar').height()
    }
});