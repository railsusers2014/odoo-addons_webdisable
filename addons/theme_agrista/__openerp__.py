{
    'name': 'Agrista Theme',
    'summary': 'The Agrista theme based on Bootstrap',
    'description': 'Based on Less theme support in Odoo',
    'category': 'Theme',
    'version': '1.0',
    'author': 'Agrista (Pty) Ltd',
    'depends': ['website'],
    'data': [
        'views/theme.xml',
        'views/theme_agrista.xml',
        'views/snippets.xml',
        'views/snippets/s_coming_soon.xml'
    ],
    'images': ['static/description/bootswatch.png'],
    'application': False,
}
