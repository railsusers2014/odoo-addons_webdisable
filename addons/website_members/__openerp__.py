{
    'name': 'Agrista Member Profiles',
    'version': '1.0',
    'category': 'Agrista',
    "sequence": 14,
    'complexity': "easy",
    'description': """
        Add public profile pages for members registered on Agrista.
    """,
    'author': 'Helmut Drewes',
    'website': 'www.agrista.com',
    'data': [
        'views/website_members.xml'
    ],
    'depends': ['website','theme_agrista','website_blog'],
    'installable': True,
    'auto_install': False,
}
