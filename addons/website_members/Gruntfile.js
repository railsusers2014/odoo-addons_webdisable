module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
            src: ['static/src/**/*.js', 'static/test/**/*.js'],
            options: {
                sub: true, //[] instead of .
                evil: true, //eval
                laxbreak: true, //unsafe line breaks
            },
        },
        sass: {
            dist: {
                options: {
                    style: "expanded",
                    trace: true
                },
                files: {
                    "static/src/css/website_members.css": "static/src/css/website_members.scss"
                }
            }
        },
        watch: {
            options: {
                livereload: true,
            },
            sass: {
                files: ["static/src/css/*.scss", "static/src/css/includes/*.scss"],
                tasks: ['sass']
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('gen', ["sass"]);
    grunt.registerTask('watcher', ["gen", "watch"]);
    grunt.registerTask('test', []);

    grunt.registerTask('default', ['jshint']);

};
